<?php
declare(strict_types = 1);

namespace App\Controllers;

use Core\Controller;

class ProductsController extends Controller
{
    public function index($pageData = NULL)
    {
        //Instanciando classes
        $ObjProduct = new $this->model($this->table);
        $obj = new Controller('products');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $allProducts = $ObjProduct->selectAll(); // Obtém todos os produtos
        $categories = $ObjCategories->selectAllForCombo();

        foreach ($allProducts['data'] AS $product){
            $product->categories_name = '';
            if($product->product_category){
                $product->product_categories_name = $ObjCategories->selectByIdsSeparatedByComma($product->product_category)->categories_name;
            }
        }

        $obj->index(
            array(
                "response" => $pageData,
                "allProducts" => $allProducts['data'],
                "initialRange" => $allProducts['initialRange'],
                "finalRange" => $allProducts['finalRange'],
                "currentPage" => $allProducts['currentPage'],
                "previousPage" => $allProducts['previousPage'],
                "nextPage" => $allProducts['nextPage'],
                "lastPage" => $allProducts['lastPage'],
                "totalRecords" => $allProducts['totalRecords'],
                "showFirstButton" => $allProducts['showFirstButton'],
                "showLastButton" => $allProducts['showLastButton'],
                "categories" => $categories
            )
        );
    }

    public function add($pageData = NULL){
        //Instanciando classes
        $obj = new Controller('products');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $categories = $ObjCategories->selectAllForCombo();

        $obj->add(
            array(
                "categories" => $categories
            )
        );
    }

    public function delete($fieldId){
        $obj = new Controller('products');
        $obj->delete($fieldId);
    }

    public function edit($fieldId = null, $pageData = NULL){
        //Instanciando classes
        $obj = new Controller('products');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $categories = $ObjCategories->selectAllForCombo();

        $obj->edit($fieldId, array(
            "categories" => $categories
        ));
    }
}
