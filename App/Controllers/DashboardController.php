<?php
declare(strict_types = 1);

namespace App\Controllers;

use Core\Controller;

class DashboardController extends Controller
{
    public function index($pageData = NULL)
    {
        //Instanciando classes
        $ObjProduct = new $this->model($this->table);
        $obj = new Controller('dashboard');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $_POST['qtd'] = $_POST['qtd']??12; //Se não conter filtro por quantidade, seta o padrão para 12
        $allProducts = $ObjProduct->selectAll(); // Obtém todos os produtos
        $categories = $ObjCategories->selectAllForCombo();

        foreach ($allProducts['data'] AS $product){
            $product->categories_name = '';
            if($product->product_category)
                $product->product_categories_name = $ObjCategories->selectByIdsSeparatedByComma($product->product_category)->categories_name;
            if($product->product_img)
                $product->product_img = URL."assets/images_uploaded/$product->product_img";
            else
                $product->product_img = URL."assets/images/sem_imagem.jpg";
        }

        $obj->index(
            array(
                "response" => $pageData,
                "allProducts" => $allProducts['data'],
                "initialRange" => $allProducts['initialRange'],
                "finalRange" => $allProducts['finalRange'],
                "currentPage" => $allProducts['currentPage'],
                "previousPage" => $allProducts['previousPage'],
                "nextPage" => $allProducts['nextPage'],
                "lastPage" => $allProducts['lastPage'],
                "totalRecords" => $allProducts['totalRecords'],
                "showFirstButton" => $allProducts['showFirstButton'],
                "showLastButton" => $allProducts['showLastButton'],
                "categories" => $categories
            )
        );
    }
}
