<?php
declare(strict_types = 1);

namespace App\Controllers;

use Core\Controller;

class CategoriesController extends Controller
{
    public function index($pageData = NULL)
    {
        //Instanciando classes
        $obj = new Controller('categories');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $allCategories = $ObjCategories->selectAll(); // Obtém todas as Categorias
        $categories = $ObjCategories->selectAllForCombo(); //Obtém as categorias para montar o combo

        $obj->index(
            array(
                "response" => $pageData,
                "allCategories" => $allCategories['data'],
                "initialRange" => $allCategories['initialRange'],
                "finalRange" => $allCategories['finalRange'],
                "currentPage" => $allCategories['currentPage'],
                "previousPage" => $allCategories['previousPage'],
                "nextPage" => $allCategories['nextPage'],
                "lastPage" => $allCategories['lastPage'],
                "totalRecords" => $allCategories['totalRecords'],
                "showFirstButton" => $allCategories['showFirstButton'],
                "showLastButton" => $allCategories['showLastButton'],
                "categories" => $categories
            )
        );
    }

    public function add($pageData = NULL){
        //Instanciando classes
        $obj = new Controller('categories');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $categories = $ObjCategories->selectAllForCombo();

        $obj->add(
            array(
                "categories" => $categories
            )
        );
    }

    public function delete($fieldId){
        $obj = new Controller('categories');
        $obj->delete($fieldId);
    }

    public function edit($fieldId = null, $pageData = NULL){
        //Instanciando classes
        $obj = new Controller('categories');
        $modelCategories = $obj->loadModel('categories');
        $ObjCategories = new $modelCategories('categories');

        //parâmetros para carregar na view
        $categories = $ObjCategories->selectAllForCombo();

        $obj->edit($fieldId, array(
            "categories" => $categories
        ));
    }
}
