<?php
declare(strict_types = 1);
namespace App\Models;

use Core\Model;

class LogsModel extends Model
{

    private $table = '';

    /**
     * Abre a conexão com o banco.
     */
    function __construct($table)
    {
        $this->table = $table;
        parent::__construct($this->table);
    }

    public function add($modelThatCaused, $tableId, $action, $description): array
    {
        $sql = "
            INSERT INTO {$this->table} (model_that_caused, table_id, action, description) 
            VALUES (:model_that_caused, :table_id, :action, :description)
        ";
        $query = $this->pdo->prepare($sql);
        $parameters = array(
            ":model_that_caused" => $modelThatCaused,
            ":table_id" => $tableId,
            ":action" => $action,
            ":description" => $description,
        );

        if ($query->execute($parameters)) {
            return array('status' => 'success', 'type' => 'success', 'msg' => 'Log criado com sucesso!');
        } else
            return array('status' => 'error', 'type' => 'danger', 'msg' => 'Erro ao criar log');
    }

    public function selectByTableId($fieldId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE table_id = :table_id LIMIT 1";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':table_id' => $fieldId);

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    public function selectByModel($model)
    {
        $sql = "SELECT * FROM {$this->table} WHERE model_that_caused = :model_that_caused";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':model_that_caused' => $model);

        $query->execute($parameters);

        return $query->fetchAll();
    }

    public function selectAll()
    {
        /* Filtros */
        $where = '';
        if($_POST){
            if(@$_POST['description'])
                $where = " WHERE description LIKE '%{$_POST['description']}%' ";
            if(@$_POST['table_id'])
                $where = " WHERE table_id = {$_POST['table_id']} ";
            if(@$_POST['action'])
                $where = " WHERE action = '{$_POST['action']}' ";
            if(@$_POST['model_that_caused'])
                $where = " WHERE model_that_caused = '{$_POST['model_that_caused']}' ";
        }

        /* Recebe o número da página via parâmetro na URL */
        $currentPage = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;

        /* Parâmetros opcionais */
        $numberRecords = (isset($_POST['qtd']) && is_numeric($_POST['qtd'])) ? $_POST['qtd'] : 10; //quantidade de registros em  cada página
        $rangePages = (isset($_POST['range']) && is_numeric($_POST['range'])) ? $_POST['range'] : 3; //range de páginas que irão aparecer na paginação

        /* Calcula a linha inicial da consulta */
        $initialLine = ($currentPage -1) * $numberRecords;

        //Query Principal para obter todos os registros
        $sql = "SELECT * FROM {$this->table} {$where} ORDER BY created_on DESC LIMIT {$initialLine}, {$numberRecords}";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        $data = $query->fetchAll();

        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_records FROM {$this->table} {$where}";
        $stm = $this->pdo->prepare($sqlContador);
        $stm->execute();
        $value = $stm->fetch();

        /* Cálcula qual será a última página */
        $lastPage  = ceil($value->total_records / $numberRecords);

        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $previousPage = ($currentPage > 1) ? $currentPage -1 : 0 ;

        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $nextPage = ($currentPage < $lastPage) ? $currentPage +1 : 0 ;

        /* Cálcula qual será a página inicial do nosso range */
        $initialRange  = (($currentPage - $rangePages) >= 1) ? $currentPage - $rangePages : 1 ;

        /* Cálcula qual será a página final do nosso range */
        $finalRange   = (($currentPage + $rangePages) <= $lastPage ) ? $currentPage + $rangePages : $lastPage ;

        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $showFirstButton = $initialRange < $currentPage;

        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $showFinalButton = ($finalRange > $currentPage);

        return array(
            "data" => $data,
            "initialRange" => $initialRange,
            "finalRange" => $finalRange,
            "currentPage" => $currentPage,
            "previousPage" => $previousPage,
            "nextPage" => $nextPage,
            "lastPage" => $lastPage,
            "totalRecords" => $value->total_records,
            "showFirstButton" => $showFirstButton,
            "showLastButton" => $showFinalButton,
        );
    }
}
