<?php
declare(strict_types = 1);
namespace App\Models;

use Core\Model;

class ProductsModel extends Model
{

    private $table = '';

    /**
     * Abre a conexão com o banco.
     */
    function __construct($table)
    {
        $this->table = $table;
        parent::__construct($this->table);
    }

    public function add($data): array
    {
        $checkIfProductExists = $this->selectById($data['sku']);
        if(!$checkIfProductExists) { //Se o produto não existe, cria o registro no banco
            //verifica se realizou upload de imagem
            if($_FILES['product_img']){
                $checkIfIsImage = getimagesize($_FILES["product_img"]["tmp_name"]);
                if(!$checkIfIsImage) //Se o arquivo não for uma imagem
                    return array('status'=> 'error', 'type' => 'danger', 'msg' => 'O arquivo deve ser uma imagem!');
                elseif ($_FILES["product_img"]["size"] > 3097152) //Se o arquivo for maior que 3mb
                    return array('status'=> 'error', 'type' => 'danger', 'msg' => 'A imagem não pode ser maior que 3mb');

                $file_name = rand(1,999999).'_'.time().'.'.explode('.',$_FILES["product_img"]['name'])[1];
                $target = "assets/images_uploaded/$file_name"; //Local onde o arquivo será armazenado
                if (!move_uploaded_file($_FILES["product_img"]["tmp_name"], $target)) //Move a imagem (OBS: Futuramente seria interessante utilizar AWS S3 para hospedar imagens, ou correlatos)
                    return array('status'=> 'error', 'type' => 'danger', 'msg' => 'Erro ao realizar o upload da imagem');
            }
            $sql = "
                INSERT INTO {$this->table} (sku, product_name, product_price, product_description, product_qty, product_category, product_img) 
                VALUES (:sku, :product_name, :product_price, :product_description, :product_qty, :product_category, :product_img)
            ";
            $query = $this->pdo->prepare($sql);
            $parameters = array(
                ":sku" => $data['sku'],
                ":product_name" => $data['product_name'],
                ":product_price" => str_replace(',', '', $data['product_price']),
                ":product_description" => $data['product_description'],
                ":product_qty" => $data['product_qty'],
                ":product_category" => !empty($data['product_category']) ? implode(',', $data['product_category']) : NULL,
                ":product_img" => @$file_name??NULL
            );

            if($query->execute($parameters)) {
                $objLog = new LogsModel('logs');
                $objLog->add(ucfirst($this->table) . 'Model', $data['sku'], 'created', 'Produto criado'); //Registra um log
                return array('status' => 'success', 'type' => 'success', 'msg' => 'Produto criado com sucesso!', 'identifier' => $data['sku'], 'view' => true);
            }else
                return array('status'=> 'error', 'type' => 'danger', 'msg' => 'Erro ao criar produto');
        }else //se o produto já existe
            return array('status'=> 'error', 'type' => 'danger', 'msg' => 'Esse produto já existe!');
    }

    public function selectById($fieldId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE sku = :sku LIMIT 1";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':sku' => $fieldId);

        $query->execute($parameters);
        $response = $query->rowcount() ? $query->fetch() : false;
        if($response){
            if($response->product_img)
                $response->product_img = URL."assets/images_uploaded/$response->product_img";
            else
                $response->product_img = URL."assets/images/sem_imagem.jpg";
        }

        return ($response);
    }

    public function update($data)
    {
        $sql = "
            UPDATE {$this->table} 
            SET product_name = :product_name, product_price = :product_price, product_description = :product_description,
                product_qty = :product_qty , product_category = :product_category 
            WHERE sku = :sku
        ";
        $query = $this->pdo->prepare($sql);
        $parameters = array(
            ":sku" => $data['sku'],
            ":product_name" => $data['product_name'],
            ":product_price" => $data['product_price'],
            ":product_description" => $data['product_description'],
            ":product_qty" => $data['product_qty'],
            ":product_category" => implode(',', $data['product_category'])
        );

        if ($query->execute($parameters)) {
            $objLog = new LogsModel('logs');
            $objLog->add(ucfirst($this->table) . 'Model', $data['sku'], 'updated', 'Produto editado'); //Registra um log
            return array('status' => 'success', 'type' => 'success', 'msg' => 'Produto editado com sucesso!');
        } else
            return array('status' => 'error', 'type' => 'danger', 'msg' => 'Erro ao editar produto');
    }

    public function selectAll()
    {
        /* Filtros */
        $where = '';
        if($_POST){
            $where = ' WHERE 1 ';
            if(@$_POST['sku'])
                $where .= " AND SKU='{$_POST['sku']}' ";
            if(@$_POST['product_name'])
                $where .= " AND product_name LIKE '%{$_POST['product_name']}%' ";
            if(@$_POST['product_category'])
                $where .= " AND product_category IN(".implode(',', $_POST['product_category']).") ";
        }

        /* Recebe o número da página via parâmetro na URL */
        $currentPage = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;

        /* Parâmetros opcionais */
        $numberRecords = (isset($_POST['qtd']) && is_numeric($_POST['qtd'])) ? $_POST['qtd'] : 10; //quantidade de registros em  cada página
        $rangePages = (isset($_POST['range']) && is_numeric($_POST['range'])) ? $_POST['range'] : 3; //range de páginas que irão aparecer na paginação

        /* Calcula a linha inicial da consulta */
        $initialLine = ($currentPage -1) * $numberRecords;

        //Query Principal para obter todos os registros
        $sql = "SELECT * FROM {$this->table} {$where} ORDER BY created_on DESC LIMIT {$initialLine}, {$numberRecords}";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        $data = $query->fetchAll();

        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_records FROM {$this->table} {$where}";
        $stm = $this->pdo->prepare($sqlContador);
        $stm->execute();
        $value = $stm->fetch();

        /* Cálcula qual será a última página */
        $lastPage  = ceil($value->total_records / $numberRecords);

        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $previousPage = ($currentPage > 1) ? $currentPage -1 : 0 ;

        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $nextPage = ($currentPage < $lastPage) ? $currentPage +1 : 0 ;

        /* Cálcula qual será a página inicial do nosso range */
        $initialRange  = (($currentPage - $rangePages) >= 1) ? $currentPage - $rangePages : 1 ;

        /* Cálcula qual será a página final do nosso range */
        $finalRange   = (($currentPage + $rangePages) <= $lastPage ) ? $currentPage + $rangePages : $lastPage ;

        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $showFirstButton = $initialRange < $currentPage;

        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $showFinalButton = ($finalRange > $currentPage);

        return array(
            "data" => $data,
            "initialRange" => $initialRange,
            "finalRange" => $finalRange,
            "currentPage" => $currentPage,
            "previousPage" => $previousPage,
            "nextPage" => $nextPage,
            "lastPage" => $lastPage,
            "totalRecords" => $value->total_records,
            "showFirstButton" => $showFirstButton,
            "showLastButton" => $showFinalButton,
        );
    }

    public function delete($sku)
    {
        $sql = 'DELETE FROM '.$this->table.' WHERE sku = :sku';
        $query = $this->pdo->prepare($sql);
        $parameters = array(':sku' => $sku);

        if($query->execute($parameters)) {
            $objLog = new LogsModel('logs');
            $objLog->add(ucfirst($this->table) . 'Model', $sku, 'deleted', 'Produto excluído'); //Registra um log
            return array('status' => 'success', 'type' => 'success', 'msg' => 'Produto excluído com sucesso!');
        }else
            return array('status'=> 'error', 'type' => 'danger', 'msg' => 'Erro ao excluir produto');
    }
}
