<?php
declare(strict_types = 1);
namespace App\Models;

use Core\Model;

class CategoriesModel extends Model
{

    private $table = '';

    /**
     * Abre a conexão com o banco.
     */
    function __construct($table)
    {
        $this->table = $table;
        parent::__construct($this->table);
    }

    public function add($data): array
    {
        $ifCategoryExists = $this->selectByCategoryName($data['category_name']);
        if(!$ifCategoryExists) { //Se a categoria não existir, permite criar
            $sql = "
                INSERT INTO {$this->table} (category_name) 
                VALUES (:category_name)
            ";
            $query = $this->pdo->prepare($sql);
            $parameters = array(
                ":category_name" => $data['category_name'],
            );

            if ($query->execute($parameters)) {
                $objLog = new LogsModel('logs');
                $objLog->add(ucfirst($this->table) . 'Model', $this->pdo->lastInsertId(), 'created', 'Categoria criada'); //Registra um log
                return array('status' => 'success', 'type' => 'success', 'msg' => 'Categoria criada com sucesso!', 'view' => false);
            } else
                return array('status' => 'error', 'type' => 'danger', 'msg' => 'Erro ao criar categoria');
        }else //se a categoria já existe
            return array('status'=> 'error', 'type' => 'danger', 'msg' => 'Essa categoria já existe!');
    }


    public function selectAllForCombo()
    {
        $sql = 'SELECT * FROM categories ORDER BY category_name ASC';
        $query = $this->pdo->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function selectById($fieldId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE category_id = :category_id LIMIT 1";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':category_id' => $fieldId);

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    public function selectByCategoryName($categoryName)
    {
        $sql = "SELECT * FROM {$this->table} WHERE category_name = :category_name LIMIT 1";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':category_name' => $categoryName);

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    public function selectByCategoryNameAndDifferentIf($categoryName, $categoryId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE category_name = :category_name AND category_id <> :category_id LIMIT 1";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':category_name' => $categoryName, ':category_id' => $categoryId);

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    public function selectByIdsSeparatedByComma($categoryIds)
    {
        $sql = "SELECT GROUP_CONCAT(category_name SEPARATOR ', ') AS categories_name FROM categories WHERE find_in_set(cast(category_id as char), :category_ids)";
        $query = $this->pdo->prepare($sql);
        $parameters = array(':category_ids' => $categoryIds);

        $query->execute($parameters);
        return $query->fetch();
    }

    public function update($data)
    {
        $ifCategoryNameExists = $this->selectByCategoryNameAndDifferentIf($data['category_name'], $data['category_id']);
        if(!$ifCategoryNameExists) { //Se o nome da categoria não existir, permite editar com novo nome
            $sql = "
                UPDATE {$this->table} 
                SET category_name = :category_name
                WHERE category_id = :category_id
            ";
            $query = $this->pdo->prepare($sql);
            $parameters = array(
                ":category_id" => $data['category_id'],
                ":category_name" => $data['category_name'],
            );

            if ($query->execute($parameters)) {
                $objLog = new LogsModel('logs');
                $objLog->add(ucfirst($this->table) . 'Model', $data['category_id'], 'updated', 'Categoria editada'); //Registra um log
                return array('status' => 'success', 'type' => 'success', 'msg' => 'Categoria editada com sucesso!');
            } else
                return array('status' => 'error', 'type' => 'danger', 'msg' => 'Erro ao editar categoria');
        }else //se o nome da categoria já existe
            return array('status'=> 'error', 'type' => 'danger', 'msg' => "A categoria {$data['category_name']} já existe!");
    }

    public function selectAll()
    {
        /* Filtros */
        $where = '';
        if($_POST){
            if(@$_POST['category_name'])
                $where = " WHERE category_name LIKE '%{$_POST['category_name']}%' ";
        }

        /* Recebe o número da página via parâmetro na URL */
        $currentPage = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;

        /* Parâmetros opcionais */
        $numberRecords = (isset($_POST['qtd']) && is_numeric($_POST['qtd'])) ? $_POST['qtd'] : 10; //quantidade de registros em  cada página
        $rangePages = (isset($_POST['range']) && is_numeric($_POST['range'])) ? $_POST['range'] : 3; //range de páginas que irão aparecer na paginação

        /* Calcula a linha inicial da consulta */
        $initialLine = ($currentPage -1) * $numberRecords;

        //Query Principal para obter todos os registros
        $sql = "SELECT * FROM {$this->table} {$where} ORDER BY created_on DESC LIMIT {$initialLine}, {$numberRecords}";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        $data = $query->fetchAll();

        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_records FROM {$this->table} {$where}";
        $stm = $this->pdo->prepare($sqlContador);
        $stm->execute();
        $value = $stm->fetch();

        /* Cálcula qual será a última página */
        $lastPage  = ceil($value->total_records / $numberRecords);

        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $previousPage = ($currentPage > 1) ? $currentPage -1 : 0 ;

        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $nextPage = ($currentPage < $lastPage) ? $currentPage +1 : 0 ;

        /* Cálcula qual será a página inicial do nosso range */
        $initialRange  = (($currentPage - $rangePages) >= 1) ? $currentPage - $rangePages : 1 ;

        /* Cálcula qual será a página final do nosso range */
        $finalRange   = (($currentPage + $rangePages) <= $lastPage ) ? $currentPage + $rangePages : $lastPage ;

        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $showFirstButton = $initialRange < $currentPage;

        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $showFinalButton = ($finalRange > $currentPage);

        return array(
            "data" => $data,
            "initialRange" => $initialRange,
            "finalRange" => $finalRange,
            "currentPage" => $currentPage,
            "previousPage" => $previousPage,
            "nextPage" => $nextPage,
            "lastPage" => $lastPage,
            "totalRecords" => $value->total_records,
            "showFirstButton" => $showFirstButton,
            "showLastButton" => $showFinalButton,
        );
    }

    public function delete($categoryId)
    {
        $sql = 'DELETE FROM '.$this->table.' WHERE category_id = :category_id';
        $query = $this->pdo->prepare($sql);
        $parameters = array(':category_id' => $categoryId);

        if($query->execute($parameters)) {
            $objLog = new LogsModel('logs');
            $objLog->add(ucfirst($this->table) . 'Model', $categoryId, 'deleted', 'Categoria excluída'); //Registra um log
            return array('status' => 'success', 'type' => 'success', 'msg' => 'Categoria excluída com sucesso!');
        }else
            return array('status'=> 'error', 'type' => 'danger', 'msg' => 'Erro ao excluir categoria');
    }
}
