-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           8.0.27 - MySQL Community Server - GPL
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Copiando estrutura para tabela mvc.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `created_by` int DEFAULT NULL COMMENT 'Futuramente poderia salvar aqui o id do usuário que realizou uma criação de registro',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL COMMENT 'Futuramente poderia salvar aqui o id do usuário que realizou uma edição do registro',
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_name` (`category_name`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Copiando dados para a tabela mvc.categories: ~19 rows (aproximadamente)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `category_name`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
	(1, 'Calçados', NULL, '2022-06-23 14:19:12', NULL, '2022-06-23 14:19:12'),
	(2, 'Roupas', NULL, '2022-06-23 14:19:12', NULL, '2022-06-23 14:19:12');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Copiando estrutura para tabela mvc.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int NOT NULL AUTO_INCREMENT,
  `model_that_caused` varchar(50) NOT NULL,
  `table_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Id do registro da tabela que foi realizada uma ação. Ex: Products',
  `action` varchar(10) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_by` int DEFAULT NULL COMMENT 'Futuramente poderia salvar aqui o id do usuário que realizou uma criação de registro',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL COMMENT 'Futuramente poderia salvar aqui o id do usuário que realizou uma edição do registro',
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Copiando dados para a tabela mvc.logs: ~0 rows (aproximadamente)
DELETE FROM `logs`;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` (`log_id`, `model_that_caused`, `table_id`, `action`, `description`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
	(1, 'ProductsModel', '3334-3239', 'updated', 'Produto editado', NULL, '2022-06-19 19:46:11', NULL, '2022-06-19 19:46:11');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Copiando estrutura para tabela mvc.products
CREATE TABLE IF NOT EXISTS `products` (
  `sku` varchar(20) NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `product_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `product_description` text NOT NULL,
  `product_qty` int NOT NULL DEFAULT '0',
  `product_category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_img` varchar(50) DEFAULT NULL,
  `created_by` int DEFAULT NULL COMMENT 'Futuramente poderia salvar aqui o id do usuário que realizou uma criação de registro',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL COMMENT 'Futuramente poderia salvar aqui o id do usuário que realizou uma edição do registro',
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sku`),
  UNIQUE KEY `sku` (`sku`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Copiando dados para a tabela mvc.products: ~996 rows (aproximadamente)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`sku`, `product_name`, `product_price`, `product_description`, `product_qty`, `product_category`, `product_img`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
	('3334-3239', 'Tênis Adidas Lite Racer 3 0 Masculino', 165.00, '<p>O T&ecirc;nis Lite Racer 3 0 Masculino, garante conforto aos p&eacute;s, performance e previne les&otilde;es pelo impacto ao usu&aacute;rio. Al&eacute;m disso, &eacute; de peso leve e com excelente custo-benef&iacute;cio, ideal para corredores que buscam uma combina&ccedil;&atilde;o entre a versatilidade e tecnologia de amortecimento. O cabedal em Mesh respir&aacute;vel e leve passa por um processo de termo-selagem, gerando melhor estrutura no cabedal, redu&ccedil;&atilde;o de costuras e calce facilitado.</p>\r\n\r\n<ul>\r\n	<li><strong>Nome:</strong>&nbsp;T&ecirc;nis Adidas Lite Racer 3 0 Masculino</li>\r\n	<li><strong>G&ecirc;nero:</strong>&nbsp;Masculino</li>\r\n	<li><strong>Departamento BS:</strong>&nbsp;Esporte</li>\r\n	<li><strong>Indicado para:</strong>&nbsp;Dia a Dia</li>\r\n	<li><strong>Estilo da Pe&ccedil;a:</strong>&nbsp;Lisa</li>\r\n	<li><strong>Material:</strong>&nbsp;Mesh</li>\r\n	<li><strong>Altura do Cano:</strong>&nbsp;Cano Baixo</li>\r\n	<li><strong>Fechamento:</strong>&nbsp;Cadar&ccedil;o</li>\r\n	<li><strong>Solado:</strong>&nbsp;Borracha</li>\r\n	<li><strong>Garantia do Fabricante:</strong>&nbsp;Contra defeito de fabrica&ccedil;&atilde;o</li>\r\n	<li><strong>Origem:</strong>&nbsp;Nacional</li>\r\n	<li><strong>Marca:&nbsp;</strong><a href="https://www.netshoes.com.br/adidas">Adidas</a></li>\r\n</ul>\r\n', 30, '2', '976223_1655667484.jpg', NULL, '2022-06-19 16:38:04', NULL, '2022-06-19 19:46:11');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
