# MVC

## Instalação

- Realize o clone do projeto, utilize o comando: git clone https://gitlab.com/rodrigo_1998/mvc.git
- Copie o projeto para o diretório web do apache
- Com um prompt de comando, entre dentro da pasta do projeto, execute o comando: composer install
- Em seguida execute: composer dump-autoload
- Importe o arquivo db.sql no banco de dados, ele irá criar o banco e as tabelas.
- Configure o banco de dados no projeto, no arquivo: Core/config.php
- Rode a aplicação no navegador: http://localhost/mvc

## Tecnologias utilizadas
- PHP versão 7.4.26
- MYSQL versão 8.0.27

##Bibliotecas Utilizadas
- Bootstrap v3.3.7
- CKeditor v4.19

## Alguns recursos
- Estrutura MVC
- CRUD  
- Cria URLs limpas e "bonitas"
- tenta seguir as diretrizes de codificação do PSR
- usa PDO para qualquer solicitação de banco de dados
- Tabelas com paginação e filtros
- Uploads de imagem localmente (Futuramente pode ser migrado para o AWS S3 ou correlatos)  
- Código comentado
- Usa apenas código PHP nativo.
- .htaccess - redireciona tudo que chega na pasta /public para /public/index.php
- index.php - Front Controller, única entrada permitida para o aplicativo, tornando-o mais seguro
- Foi utilizado herança, com isso há um maior reaproveitamento de código e uma maior modularidade e organização

## Segurança
O script usa o mod_rewrite e bloqueia todo o acesso de fora da pasta/public.