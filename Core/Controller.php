<?php
declare(strict_types = 1);

namespace Core;

class Controller
{
    public $table = null;
    protected $model = '';

    public function __construct($table='products'){
        $this->table = $table;
        $this->model = '\\App\\Models\\'.ucfirst($this->table) . 'Model';
    }

    public function loadModel($table = 'products'){
        return '\\App\\Models\\'.ucfirst($table) . 'Model';
    }

    public function loadController($table = 'products'){
        return '\\App\\Controllers\\'.ucfirst($table) . 'Controller';
    }

    public function index($pageData = NULL)
    {
        new Model($this->table);

        // Carregar views.
        require_once APP . 'Views/_includes/header.phtml';
        require_once APP . 'Views/_includes/menu.phtml';
        require_once APP . 'Views/'.$this->table.'/index.phtml';
        require_once APP . 'Views/_includes/footer.phtml';
    }

    public function add($pageData = NULL)
    {
        if (isset($_POST['submit_add_'.$this->table])) {
            $obj = new $this->model($this->table);
            $response = $obj->add($_POST);
            if($response['status'] == 'success' && $response['view'] == true && isset($response['identifier'])) //Se criou e se deseja carregar a view
                header('location: ' . URL . "{$this->table}/view/{$response['identifier']}&msg=1");
            elseif($response['status'] == 'success')
                header('location: ' . URL . "{$this->table}&msg=2");
        }

        // Carregar views.
        require_once APP . 'Views/_includes/header.phtml';
        require_once APP . 'Views/_includes/menu.phtml';
        require_once APP . 'Views/'.$this->table.'/add.phtml';
        require_once APP . 'Views/_includes/footer.phtml';
    }

    public function view($fieldId = null, $pageData = NULL)
    {
        if (isset($fieldId)) {
            $obj = new $this->model($this->table);
            $register = $obj->selectById($fieldId);

            if ($register === false) { //se o registro não existir
                $page = new \Core\ErrorController();
                $page->index(2, 'Ops!<br/> Esse registro não existe!');
            } else { //carrega view
                require_once APP . 'Views/_includes/header.phtml';
                require_once APP . 'Views/_includes/menu.phtml';
                require_once APP . 'Views/'.$this->table.'/view.phtml';
                require_once APP . 'Views/_includes/footer.phtml';
            }
        } else {
            header('location: ' . URL . $this->table);
        }
    }

    public function edit($fieldId = null, $pageData = NULL)
    {
        if (isset($fieldId)) {
            $obj = new $this->model($this->table);
            $register = $obj->selectById($fieldId);

            if ($register === false) { //se o registro não existir
                $page = new \Core\ErrorController();
                $page->index(2, 'Ops!<br/> Esse registro não existe!');
            } elseif (isset($_POST["submit_update_{$this->table}"])) { //se realizou a ação para atualizar
                $response = $obj->update($_POST);
                if($response['status'] == 'success')
                    header('location: ' . URL . $this->table.'&msg=1');
                else {
                    require_once APP . 'Views/_includes/header.phtml';
                    require_once APP . 'Views/_includes/menu.phtml';
                    require_once APP . 'Views/' . $this->table . '/edit.phtml';
                    require_once APP . 'Views/_includes/footer.phtml';
                }
            } else { //carrega view
                require_once APP . 'Views/_includes/header.phtml';
                require_once APP . 'Views/_includes/menu.phtml';
                require_once APP . 'Views/'.$this->table.'/edit.phtml';
                require_once APP . 'Views/_includes/footer.phtml';
            }
        } else {
            header('location: ' . URL . $this->table);
        }
    }

    public function delete($fieldId)
    {
        $obj = new $this->model($this->table);
        $response = null;
        if (isset($fieldId)) {
            $response = $obj->delete($fieldId);
        }
        $controller = $this->loadController($this->table);
        $obj = new $controller($this->table);
        $obj->index($response);
    }
}
