<?php
namespace Core;

class Model extends Connection
{

    private $table = '';
    public $pdo = null;

    function __construct($table)
    {
        $this->table = $table;
        try {
            self::openDb();
        } catch ( PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    public function class($sufix){
      $class = ucfirst($this->table).$sufix."('$this->table')";
      return $class;
    }
}
